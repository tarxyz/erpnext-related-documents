### 安装步骤

1. ubuntu 20.04.2 server，安装系统时设置的用户名是frappe，使用root账号登录系统。

2. 备份镜像源文件，替换成国内镜像源，更新系统，重启。

3. 下载easy-install脚本，这个脚本是在官方脚本基本上将github的源改成了gitee源，默认安装版本由12版改成了13版。

   ```
   wget https://gitee.com/qinyanwan/erpnext/attach_files/870219/download/install.py
   ```

4. 安装python3和stuptool包

   ```
   apt install python3-minimal build-essential python3-setuptools
   ```

5. 提前系统语言环境修改一下，否则安装过程可能会报错，提示你修改。

   ```
    export LC_ALL=C.UTF-8
   ```

6. 安装

   ```
   python3 install.py --production --user frappe
   ```



### 说明

1. 安装过程中提示设置MySQL密码和登录ERPNext的超级管理员administrator的密码，按要求设置好。

   ```
   Input MySQL and Frappe Administrator passwords:
   Please enter mysql root password: 
   Re-enter mysql root password: 
   
   Please enter the default Administrator user password: 
   Re-enter Administrator password: 
   ```

2. 安装过程中会有一些Warning，不用理会，只要不报错不退出就耐心等待。

3. 最后见到“Bench+Frappe+Erpnext has been successfully installed”说明系统安装成功，通过浏览器访问服务器的IP地址即可登陆ERPNEXT。。 



### install.py的其他一些常见参数

1. --production参数，指定安装脚本install.py安装当前的稳定版，如果需要安装开发版可以使用--develop参数；
2. --user, 指定系统用户名（如果没有指定用户名，系统默认创建一个frappe用户，且把erpnext系统都安装到该用户目录下，因安装系统时已经建了frappe用户，直接指定frappe就好）
3. --version 指定安装的系统版本，默认安装当前的稳定版 13，version参数可以安装指定版本，比如version-13-beta
